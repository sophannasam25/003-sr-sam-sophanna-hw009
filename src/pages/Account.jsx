import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
} from "react-router-dom";
import { Table } from "react-bootstrap";
export default function Account() {
  let { id } = useParams();

  return (
    <div>
      <h2>Accounts</h2>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>A</th>
            <th>B</th>
            <th>C</th>
            <th>D</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Link to="/account/netflix">Netflix</Link>
            </td>
            <td>
              {" "}
              <Link to="/account/zillow-group">Zillow Group</Link>
            </td>
            <td>
              <Link to="/account/yahoo">Yahoo</Link>
            </td>
            <td>
              <Link to="/account/modus-create">Modus Create</Link>
            </td>
          </tr>
        </tbody>
      </Table>
      <h3>
        ID: <span className="id">{id}</span>
      </h3>
    </div>
  );
}

import React from 'react'
import { useParams } from 'react-router'
export default function Detail() {
    let param=useParams()
    console.log("Params",param)
    return (
        <div>
            <h3>Detail: {param.id}</h3>
        </div>
    )
}

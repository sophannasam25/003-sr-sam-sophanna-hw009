import React,{Component} from 'react'
import {Redirect} from 'react-router'
export default function ProtectedRoute({isSignin,component:Component,path}) {
   if(isSignin){
        return <Component path={path}/>
   }else{
       return <Redirect to="/welcome" />
   }
}

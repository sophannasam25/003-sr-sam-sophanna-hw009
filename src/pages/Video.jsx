import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
  useLocation,
} from "react-router-dom";
import queryString from "query-string";
import { Card, Button, Row, ButtonGroup } from "react-bootstrap";
export default function Nested() {
  return (
    <Router>
      <div>
        <h1>Video</h1>
        <ButtonGroup aria-label="Basic example">
          <Button variant="secondary">
            <Link to="/video/animation">Animation</Link>
          </Button>
          <Button variant="secondary">
            <Link to="/video/movie">Movie</Link>
          </Button>
        </ButtonGroup>
        <Switch>
          <Route path="/video/animation">
            <Animation />
          </Route>

          <Route path="/video/movie">
            <Movie />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Animation() {
  const { path, url } = useRouteMatch();
  const [name, setName] = useState("nita");
  return (
    <div>
      <h2>Animation Category</h2>
      <div
        class="btn-group"
        role="group"
        aria-label="Basic mixed styles example"
      >
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Action`}>Action</Link>
        </button>
        {/* ?name=? */}
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Romance`}>Romance</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Comedy`}>Comedy</Link>
        </button>
      </div>

      <Switch>
        <Route path={`${path}/:animationId`}>
          <Animations />
        </Route>
      </Switch>
    </div>
  );
}

function Animations() {
  let { animationId } = useParams();
  return (
    <div>
      <h3>
        Please Choose Category:
        <span className="animationId">{animationId}</span>
      </h3>
    </div>
  );
}

function Movie() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <h2>Animation Category</h2>
      <div
        class="btn-group"
        role="group"
        aria-label="Basic mixed styles example"
      >
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Action`}>Action</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Romance`}>Romance</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Comedy`}>Comedy</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Action`}>Action</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Romance`}>Romance</Link>
        </button>
        <button type="button" class="btn btn-secondary">
          <Link to={`${url}/Horror`}>Horror</Link>
        </button>
      </div>

      <Switch>
        <Route path={`${path}/:animation1Id`}>
          <Animationss />
        </Route>
      </Switch>
    </div>
  );
}

function Animationss() {
  let { animation1Id } = useParams();
  return (
    <div>
      <h3>
        Please Choose Category:
        <span className="animationId">{animation1Id}</span>
      </h3>
    </div>
  );
}

// import React from "react";
// import {
//   BrowserRouter as Router,
//   Switch,
//   Route,
//   Link,
//   useParams,
//   useRouteMatch,
//   useLocation
// } from "react-router-dom";
// import { Button } from "react-bootstrap";
// export default function NestingExample() {
//   return (
//     <Router>
//       <div>
//         <h1>Video</h1>
//       <Button variant="secondary">
//         <Link to="/video">Home</Link>
//            {/* <Link to="/animation">Animation</Link> */}
//        </Button>

//        <Button variant="secondary">
//           <Link to="/video/animation">Animation</Link>
//          </Button>

//         <Switch>
//           <Route exact path="/home">
//             <Home />
//           </Route>
//           <Route path="/topics">
//             <QueryParamsDemo />
//           </Route>

//               <QueryParamsDemo />

//         </Switch>
//       </div>
//     </Router>
//   );
// }
// function useQuery() {
//   return new URLSearchParams(useLocation().search);
// }

// function QueryParamsDemo() {
//   let query = useQuery();

//   return (
//     <div>
//       <div>
//         <h2>Animation Category</h2>
//         <Button variant="secondary">
//              <Link to="/video/animation?type=action">Action</Link>
//         </Button>
//         <Button variant="secondary">
//             <Link to="/video/animation?type=romance">Romance</Link>
//         </Button>

//         <Button variant="secondary">
//            <Link to="/video/animation?type=comedy">Comedy</Link>
//         </Button>

//           {/* <li>
//             <Link to="/video/animation?name=modus-create">Modus Create</Link>
//           </li> */}

//         <Child name={query.get("type")} />
//       </div>
//     </div>
//   );
// }

// // function Home() {
// //   let query = useQuery();
// //   return (
// //     <div>
// //       <div>
// //         <h2>Animation Category</h2>
// //         <Button variant="secondary">
// //              <Link to="/video/homes?type=action">Action</Link>
// //         </Button>
// //         <Button variant="secondary">
// //             <Link to="/video/homes?type=romance">Romance</Link>
// //         </Button>

// //         <Button variant="secondary">
// //            <Link to="/video/homes?type=comedy">Comedy</Link>
// //         </Button>
// //           {/* <li>
// //             <Link to="/video/animation?name=modus-create">Modus Create</Link>
// //           </li> */}
// //         <Child name={query.get("type")} />
// //       </div>
// //     </div>
// //   );
// // }
// function Home() {
//   return (
//     <div>
//       <h2>dsdfdsf</h2>
//     </div>
//   );
// }
// // function Home({ name }) {
// //   return (
// //     <div>
// //       {name ? (
// //         <h3>
// //           The <code>name</code> in the query string is &quot;{name}
// //           &quot;
// //         </h3>
// //       ) : (
// //         <h3>There is no name in the query string</h3>
// //       )}
// //     </div>
// //   );
// // }
// function Child() {
//   let {name}=useParams()
//   return (
//     <div>
//       <h3 className="name">Hello:{name}</h3>

//     </div>
//   );
// }
// // function Topic() {
// //   let { topicId } = useParams();
// //   return (
// //     <div>
// //       <h3>{topicId}</h3>
// //     </div>
// //   );
// // }

import "./App.css";
import React,{useState} from "react"
import Menu from "./components/Menu";
import Account from "./pages/Account";
import Home from "./pages/Home";
import Video from "./pages/Video";
import Welcome from "./pages/Welcome";
import Auth from "./pages/Auth";
import Detail from "./pages/Detail";
import ProtectedRoute from "./pages/ProtectedRoute";
import { Container } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  const [isSignin,setSignin]=useState(true)
  function onSignin(){
    setSignin(!isSignin)
  }
  return (
    <Container> 
      <Router>
      <Menu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/video" component={Video} />
          <Route path="/account/:id" component={Account} />
          <Route path="/welcome" component={Welcome} />
          
          {/* <Route render={()=><Welcome onSignin={onSignin} isSignin={isSignin}/>}/> */}
          <Route path="/auth" component={Auth} />
          {/* <ProtectedRoute isSignin={isSignin} component={Auth} path="/auth"/> */}
          <Route path="/detail/:id" component={Detail} />
          
          <Route>
            <Detail />
          </Route>

          <Route path="/:id" children={<Account />} />
        </Switch>
      </Router>
    </Container>
  );
}

export default App;
